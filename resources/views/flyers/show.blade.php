@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <h1>{{ $flyer->street }}</h1>
            <h4>{{ $flyer->price() }}</h4>
            <hr>
            <div class="description">{!!  nl2br($flyer->description) !!}</div>
        </div>
        <div class="col-md-8">
            @foreach($flyer->photos->chunk(4) as $set)
                <div class="row">
                    @foreach($set as $photo)
                        <div class="col-md-3">
                            <img src="/{{ $photo->thumbnail }}" class="gallery-img">
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
    <hr>
    <h3>Add your photos:</h3>
    {{ Form::open(['route' => ['flyers.addphotos', 'zip' => $flyer->zip, 'street' => str_replace(' ', '-', $flyer->street)], 'class' => 'dropzone', 'method' => 'POST', 'id' => 'addPhotosForm']) }}
    {{ Form::close() }}
@stop

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css" type="text/css" rel="stylesheet">
@stop

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js"></script>
    <script>
        Dropzone.options.addPhotosForm = {
            paramName: 'photo',
            maxFilesize: 5,
            acceptedFiles: '.jpeg, .jpg, .png'
        }
    </script>
@stop