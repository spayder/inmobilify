@extends('layouts.app')

@section('content')
    <h1>Selling your home?</h1>
    <hr>
    {{ Form::open(['method' => 'POST', 'route' => 'flyers.store']) }}
    @include('errors')

        @include('flyers.form')
    {{ Form::close() }}
@stop