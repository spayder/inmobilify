<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Photo extends Model
{

    protected $fillable = ['filename', 'thumbnail'];
    protected $basePath = 'images/photos';

    public function flyer()
    {
        return $this->belongsTo(Flyer::class);
    }

    /**
     * @param UploadedFile $file
     * @return static
     */
    public static function fromRequest(UploadedFile $file)
    {
        $photo = new static;

        return $photo->saveAs($file->getClientOriginalName());
    }

    protected function saveAs($name)
    {
        $this->filename = sprintf("%s/%s", $this->basePath, $name);
        $this->thumbnail = sprintf("%s/tn-%s", $this->basePath, $name);

        return $this;
    }

    public function move(UploadedFile $file)
    {
        $file->move($this->basePath, $this->filename);

        $this->makeThumbnail();

        return $this;
    }

    protected function makeThumbnail()
    {
        Image::make($this->filename)
            ->fit(200)
            ->save($this->thumbnail);
    }
}
